//
//  Extensions.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import UIKit

extension UITableView {
    var visibleRows: ClosedRange<Int>? {
        guard let visibleRows = indexPathsForVisibleRows, let startIndex = visibleRows.first?.row, let endIndex = visibleRows.last?.row else { return nil }
        return startIndex...endIndex
    }
    
    func registerNibForCell(names: [String]) {
        for name in names {
            register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
        }
    }
}

extension UICollectionView {
    func registerNibForCell(names: [String]) {
        for name in names {
            register(UINib(nibName: name, bundle: nil), forCellWithReuseIdentifier: name)
        }
    }
}

extension UIResponder {
    static var id: String {
        return String(describing: self)
    }
}

extension UIColor {
    static func randomColor() -> UIColor {
        let red = CGFloat.random(in: 0.0...1.0)
        let green = CGFloat.random(in: 0.0...1.0)
        let blue = CGFloat.random(in: 0.0...1.0)
        return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    }
}

extension String {
    static var randomText: String {
        String((10...999).randomElement()!)
    }

}
