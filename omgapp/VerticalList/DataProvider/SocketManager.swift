//
//  SocketManager.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import Foundation

// Simulate update from socket.
final class SocketManager {
    private var timer: Timer?
    var updateReceived: (() -> Void)?

    func startTimer() {
        timer?.invalidate()
        
        let timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        // When scrolling a UIScrollView, the run loop mode can change, which can affect timer.
        // Add in Common To ensure that your timer continues to fire even during scrolling,

        RunLoop.main.add(timer, forMode: .common)
        self.timer = timer
    }
    
    @objc private func update() {
        updateReceived?()
    }
}
