//
//  DataProvider.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import Foundation

final class DataProvider {
    private var task: Task<Item, Never>?
    
    func itemsFor(row: Int) async throws -> [Item] {
        let start = row
        let end = row + 20
        let items = (start..<end).map { Item(id: "\($0)") }
        
        try await Task.sleep(nanoseconds: 500_000_000) // Simulate data response from server 0.5 sec
        return items
    }
}
