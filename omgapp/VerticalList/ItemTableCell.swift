//
//  ItemTableCell.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import UIKit

final class ItemTableCell: UITableViewCell {
    
    @IBOutlet private var collectionView: UICollectionView!
    private(set) var viewModel: ItemViewModel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        collectionView.reloadData()
        collectionView.setContentOffset(.zero, animated: false)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.registerNibForCell(names: [ItemCollectionViewCell.id])
    }
    
    func setViewModel(_ model: ItemViewModel) {
        viewModel = model
        viewModel.receivedUpdate = { [weak self] in
            self?.updateOneItem()
        }
    }
    
    private func updateOneItem() {
        viewModel.collectionModels.randomElement()!.updateText()
        guard let visibleItem = self.collectionView.indexPathsForVisibleItems.randomElement() else { return }
        
        collectionView.reloadItems(at: [visibleItem])
    }
}

extension ItemTableCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemCollectionViewCell.id, for: indexPath) as! ItemCollectionViewCell
        cell.setViewModel(viewModel.collectionModels[indexPath.row])
        return cell
    }
}

extension ItemTableCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        viewModel.collectionModels[indexPath.row].startAnimation()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.collectionModels[indexPath.row].endAnimation()
    }
}

extension ItemTableCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 50)
    }
}
