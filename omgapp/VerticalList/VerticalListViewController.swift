//
//  VerticalListViewController.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import UIKit


/*

 Note It can also be done using UITableViewDiffableDataSource, which is easier because we only need to update the data source.
 
*/


final class VerticalListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    private var viewModel = ItemsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNibForCell(names: [ItemTableCell.id])
        viewModel.viewAppeared()
        viewModel.didReceivedNewItems = { [weak self] items, index in
            let indexPaths = items.enumerated().map { (i, _) in IndexPath(row: index + i, section: 0) }
            self?.tableView.insertRows(at: indexPaths, with: .bottom)
        }
        
        viewModel.didReceivedUpdate = { [weak self] in
            // Find visible item view models
            guard let self, let visibleRows = tableView.visibleRows else { return }
            for itemViewModel in self.viewModel.items[visibleRows] {
                itemViewModel.didReceivedUpdate()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDisappeared()
    }
}

extension VerticalListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemTableCell.id, for: indexPath) as! ItemTableCell
        let itemViewModel = viewModel.items[indexPath.row]
        cell.setViewModel(itemViewModel)
        
        return cell
    }
}


extension VerticalListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let visibleRowsIndexPaths = tableView.indexPathsForVisibleRows

        // If the current row being displayed is the last row, trigger the data fetch again
        guard let lastIndexPath = visibleRowsIndexPaths?.last else { return }
            
        if lastIndexPath.row == viewModel.items.count - 1 {
            viewModel.fetchData()
        }
    }
}
