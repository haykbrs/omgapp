//
//  ItemCollectionViewCell.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import UIKit

final class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private var titleLabel: UILabel!
    private var viewModel: CollectionViewModel!

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .lightGray
        layer.borderWidth = 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height/2
    }
    
    func setViewModel(_ viewModel: CollectionViewModel) {
        self.viewModel = viewModel
        
        layer.borderColor = viewModel.color.cgColor
        titleLabel.text = viewModel.text
        
        viewModel.animationStart = { [weak self] in
            self?.startAnimation()
        }
        
        viewModel.animationFinish = { [weak self] in
            self?.endAnimation()
        }
        
        viewModel.textUpdated = { [weak self] text in
            self?.titleLabel.text = viewModel.text
        }
    }
    
    private func startAnimation() {
        UIView.animate(withDuration: 0.3) {
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
    }
    
    private func endAnimation() {
        UIView.animate(withDuration: 0.3) {
            self.transform = .identity
        }
    }
}
