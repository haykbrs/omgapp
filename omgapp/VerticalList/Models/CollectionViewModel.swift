//
//  CollectionViewModel.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import UIKit

class CollectionViewModel {
    var text: String
    let color: UIColor
    
    var animationStart: (() -> Void)?
    var animationFinish: (() -> Void)?
    var textUpdated: ((String) -> Void)?

    init() {
        text = String.randomText
        color = UIColor.randomColor()
    }
    
    func updateText() {
        text = String.randomText
        textUpdated?(text)
    }
    
    func startAnimation() {
        animationStart?()
    }
    
    func endAnimation() {
        animationFinish?()
    }
}
