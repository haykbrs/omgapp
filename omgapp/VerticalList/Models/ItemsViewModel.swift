//
//  ItemsViewModel.swift
//  omgapp
//
//  Created by Hayk Brsoyan on 11.03.24.
//

import Foundation

struct Item {
    let id: String
    
    init(id: String) {
        self.id = id
    }
}

final class ItemViewModel {
    let item: Item
    let collectionModels: [CollectionViewModel]
    
    let numberOfItems: Int
    var receivedUpdate: (() -> Void)?
    
    init(item: Item) {
        self.item = item
        self.numberOfItems = (10...100).randomElement()!
        self.collectionModels = (1...numberOfItems).map { _ in CollectionViewModel() }
    }
    
    func didReceivedUpdate() {
        receivedUpdate?()
    }
}

final class ItemsViewModel {
    private let dataManager: DataProvider
    private let socketManager: SocketManager
    
    private var isLoadingData = false
    private var task: Task<(),Never>?
    
    private(set) var items: [ItemViewModel] = []

    var didReceivedNewItems: (([ItemViewModel], Int) -> Void)?
    var didReceivedUpdate: (() -> Void)?

    init() {
        dataManager = DataProvider()
        socketManager = SocketManager()
    }
   
    func viewAppeared() {
        fetchData()
        
        socketManager.updateReceived = { [weak self] in
            self?.didReceivedUpdate?()
        }
        socketManager.startTimer()
    }
    
    func viewDisappeared() {
        task?.cancel()
    }
    
    func fetchData() {
        guard !isLoadingData else { return }
        isLoadingData = true
        
        task = Task() {
            do {
                try Task.checkCancellation()
                let newItems = try await dataManager.itemsFor(row: items.count).map { ItemViewModel(item: $0) }
                
                let index = items.count
                let items = self.items + newItems
                
                try Task.checkCancellation()
                await MainActor.run {
                    self.isLoadingData = false
                    self.items = items
                    self.didReceivedNewItems?(newItems, index)
                }
                
            } catch {
                print("Error fetching items: \(error)")
            }
        }
    }
}
